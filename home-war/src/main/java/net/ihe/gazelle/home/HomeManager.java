package net.ihe.gazelle.home;

import net.ihe.gazelle.locale.LocaleSelector;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.io.Serializable;

/**
 * <p>net.ihe.gazelle.home.HomeManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@ManagedBean(name = "homeManagerBean")
@ViewScoped
public class HomeManager implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1111092307583013570L;

	private Home selectedHome = null;
	private boolean mainContentEditMode;
	private boolean editTitle = false;

	@Inject
	private HomeTransactionDAO homeTransactionDAO;

	@Inject
	private LocaleSelector localeSelector;

	/**
	 * <p>Getter for the field <code>selectedHome</code>.</p>
	 *
	 * @return a {@link Home} object.
	 */
	public Home getSelectedHome() {
		//TODO This will need an interceptor for a "LocalIsChanged" event
		if (localeSelector.getLocalChanged()){
			selectedHome = homeTransactionDAO.getHomeForSelectedLanguage();
			localeSelector.homeReloadedWithLocalChanges();
		}
		return selectedHome;
	}

	/**
	 * <p>isEditTitle.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isEditTitle() {
		return editTitle;
	}

	/**
	 * <p>Setter for the field <code>editTitle</code>.</p>
	 *
	 * @param editTitle a boolean.
	 */
	public void setEditTitle(boolean editTitle) {
		this.editTitle = editTitle;
	}

	/**
	 * <p>Setter for the field <code>selectedHome</code>.</p>
	 *
	 * @param selectedHome a {@link Home} object.
	 */
	public void setSelectedHome(Home selectedHome) {
		this.selectedHome = selectedHome;
	}

	/**
	 * <p>isMainContentEditMode.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isMainContentEditMode() {
		return mainContentEditMode;
	}

	/**
	 * <p>Setter for the field <code>mainContentEditMode</code>.</p>
	 *
	 * @param mainContentEditMode a boolean.
	 */
	public void setMainContentEditMode(boolean mainContentEditMode) {
		this.mainContentEditMode = mainContentEditMode;
	}

	/**
	 * <p>initializeHome.</p>
	 */
	@PostConstruct
	public void initializeHome() {
		selectedHome = homeTransactionDAO.getHomeForSelectedLanguage();
	}

	/**
	 * <p>editMainContent.</p>
	 */
	public void editMainContent() {
		mainContentEditMode = true;
	}

	/**
	 * <p>save.</p>
	 */
	public void save() {
		selectedHome = homeTransactionDAO.saveHome(selectedHome);
		mainContentEditMode = false;
		editTitle = false;

		FacesContext facesContext = FacesContext.getCurrentInstance();
		FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO,"Saved Home content", null);
		facesContext.addMessage(null, facesMessage);

	}

	/**
	 * <p>cancel.</p>
	 */
	public void cancel() {
		mainContentEditMode = false;
		editTitle = false;
	}
}
