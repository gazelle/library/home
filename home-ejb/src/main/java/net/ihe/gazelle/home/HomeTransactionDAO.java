package net.ihe.gazelle.home;

import javax.transaction.Transactional;

public interface HomeTransactionDAO {
    Home getHomeForSelectedLanguage();

    @Transactional
    Home saveHome(Home home);
}
