package net.ihe.gazelle.home;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;

import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Named("homeTransactionDAO")
public class HomeTransactionDAOImpl implements HomeTransactionDAO {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * <p>getHomeForSelectedLanguage.</p>
     *
     * @return a {@link Home} object.
     */
    @Override
    public Home getHomeForSelectedLanguage() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String language = facesContext.getViewRoot().getLocale().getISO3Language();
        if (language != null && !language.isEmpty()) {
            HQLQueryBuilder<Home> queryBuilder = new HQLQueryBuilder<Home>(entityManager, Home.class);
            queryBuilder.addEq("iso3Language", language);
            List<Home> homes = queryBuilder.getList();
            if (homes != null && !homes.isEmpty()) {
                return homes.get(0);
            } else {
                return new Home(language);
            }
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    public Home saveHome(Home home){
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        Home mergedHome = entityManager.merge(home);
        entityManager.flush();
        return mergedHome;
    }
}
