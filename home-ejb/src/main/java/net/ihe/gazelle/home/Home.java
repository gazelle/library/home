package net.ihe.gazelle.home;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.hibernate.annotations.Type;

import javax.faces.context.FacesContext;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
/**
 * <p>Home class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Table(name="cmn_home", schema="public", uniqueConstraints=@UniqueConstraint(columnNames="iso3_language"))
@SequenceGenerator(name="cmn_home_sequence", sequenceName="cmn_home_id_seq", allocationSize=1)
public class Home implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1218389994561421605L;

	@Id
	@GeneratedValue(generator="cmn_home_sequence", strategy=GenerationType.SEQUENCE)
	@NotNull
	@Column(name="id", nullable=false, unique=true)
	private Integer id;
	
	@Column(name="iso3_language")
	private String iso3Language;
	
	@Column(name="main_content")
	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String mainContent;
	
	@Column(name="home_title")
	private String homeTitle;
	
	/**
	 * <p>Constructor for Home.</p>
	 */
	public Home() {}
	
	/**
	 * <p>Constructor for Home.</p>
	 *
	 * @param iso3Language a {@link java.lang.String} object.
	 */
	public Home(String iso3Language)
	{
		this.iso3Language = iso3Language;
	}
	
	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * <p>Setter for the field <code>id</code>.</p>
	 *
	 * @param id a {@link java.lang.Integer} object.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * <p>Getter for the field <code>iso3Language</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getIso3Language() {
		return iso3Language;
	}

	/**
	 * <p>Setter for the field <code>iso3Language</code>.</p>
	 *
	 * @param iso3Language a {@link java.lang.String} object.
	 */
	public void setIso3Language(String iso3Language) {
		this.iso3Language = iso3Language;
	}

	/**
	 * <p>Getter for the field <code>mainContent</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getMainContent() {
		return mainContent;
	}

	/**
	 * <p>Setter for the field <code>mainContent</code>.</p>
	 *
	 * @param mainContent a {@link java.lang.String} object.
	 */
	public void setMainContent(String mainContent) {
		this.mainContent = mainContent;
	}

	/**
	 * <p>Getter for the field <code>homeTitle</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getHomeTitle() {
		return homeTitle;
	}

	/**
	 * <p>Setter for the field <code>homeTitle</code>.</p>
	 *
	 * @param homeTitle a {@link java.lang.String} object.
	 */
	public void setHomeTitle(String homeTitle) {
		this.homeTitle = homeTitle;
	}

	/**
	 * <p>save.</p>
	 *
	 * @return a {@link Home} object.
	 */
	public Home save()
	{
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		Home home = entityManager.merge(this);
		entityManager.flush();
		return home;
	}
	
	/**
	 * <p>getHomeForSelectedLanguage.</p>
	 *
	 * @return a {@link Home} object.
	 */
	public static Home getHomeForSelectedLanguage()
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String language = facesContext.getViewRoot().getLocale().getISO3Language();
		if (language != null && !language.isEmpty())
		{
			EntityManager entityManager = EntityManagerService.provideEntityManager();
			HQLQueryBuilder<Home> queryBuilder = new HQLQueryBuilder<Home>(entityManager, Home.class);
			queryBuilder.addEq("iso3Language", language);
			List<Home> homes = queryBuilder.getList();
			if (homes != null && !homes.isEmpty())
			{
				return homes.get(0);
			}
			else
			{
				return new Home(language);
			}
		}
		else
		{
			return null;
		}
	}
}
